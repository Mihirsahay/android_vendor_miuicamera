Copyright (C) 2021 crDroid Android Project

MiuiCamera by crDroid Android
=========================================

This repository can be used to ship Xiaomi's MiuiCamera
app on custom ROMs for Xiaomi devices.

Cheatcodes are already present for most Xiaomi devices.

All you need to do:
1. Include below config file in your device.mk
$(call inherit-product, vendor/miuicamera/config.mk)

2. Resolve your sepolicy and ensure you have dependent Misys blobs (for watermark). 
   Some reference commits are below:
   https://github.com/crdroidandroid/android_device_xiaomi_sm6150-common/commit/d80bf5e476ade9bada80bc1c1fae33579293d3c6
   https://github.com/crdroidandroid/android_device_xiaomi_sm6150-common/commit/da711e12ef8331cf5c048f83db38e043f1989701

Credits
-----------------------------------------
* ANXCamera[https://camera.aeonax.com/]
* MIUI-Core-Magisk-Module[https://github.com/reiryuki/MIUI-Core-Magisk-Module]
