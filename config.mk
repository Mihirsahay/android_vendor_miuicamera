PRODUCT_SOONG_NAMESPACES += \
    vendor/miuicamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/framework/framework-ext-res,$(TARGET_COPY_OUT_SYSTEM)/framework/framework-ext-res) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/lib,$(TARGET_COPY_OUT_SYSTEM)/lib) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/priv-app/MiuiCamera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib)

ifneq ($(TARGET_DISABLE_MIMOJI_FILES), true)
PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/mimoji/model,$(TARGET_COPY_OUT_SYSTEM)/etc/ANXCamera/mimoji) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/mimoji/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib)
endif

PRODUCT_PACKAGES += \
    MiuiCamera \
    MiuiExtraPhoto \
    miui \
    miuires \
    miuisystem \
    miuiframework \
    miuicamframework

PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.lens.oem_camera_package=com.android.camera
